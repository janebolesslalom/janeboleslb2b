loader(){
  PID=$passedPID #simulate a long process
    #echo "PID IS $PID"
    sleep 3
    echo "${green}THIS MAY TAKE A WHILE, CREATING SCRATCH ORG...${reset}"
    printf "${cyan}["
    # While process is running...
    while [ "$PID" != "" ]; do 
        if ps -p $PID > /dev/null
            then printf  "${cyan}▓"
                sleep 1
            else printf ""
                PID=""
        fi; 
    done
    echo ""
    echo ""
    echo "${cyan}Scratch org creation has completed! 🎉🎉🎉"
    echo "Opening your new LB2B scratch org now!"
    sfdx force:org:open
    echo ""
}
createScratchorg(){
    scratchOrgFile="lightningScratchOrg-def.json"
    newJSON=$(jq --arg scratchOrgAlias "$scratchOrgAlias" '.orgName= $scratchOrgAlias' lightningScratchOrg-def.json)
    echo "$newJSON" > "$scratchOrgFile" 
    newJSON=$(jq --arg scratchOrgAlias "$scratchOrgAlias" --arg devhubuser "$devhubuser" '.username=$devhubuser+"."+$scratchOrgAlias' lightningScratchOrg-def.json)
    echo "$newJSON" > "$scratchOrgFile"
    sfdx force:org:create -f lightningScratchOrg-def.json --setalias $scratchOrgAlias --durationdays 30 --wait 10
    sfdx config:set defaultusername=$devhubuser"."$scratchOrgAlias 
}
yellow=`tput setaf 3`
cyan=`tput setaf 6`
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
sfdx force
echo "${cyan}WELCOME TO B2B LIGHTNING GUIDED SETUP${reset}"
echo "${red}THIS SCRIPT REQUIRES THE SALESFORCE CLI AND THE JQ PACKAGE TO BE INSTALLED${reset}"
echo "${red}PLEASE INSTALL BOTH${reset}"
echo "${red}brew install jq${reset}"
echo ""
sfdx force:org:list --all
echo "${green}PLEASE ENTER YOUR DEVHUB USERNAME:${reset}"
read devhubuser
echo "${green}setting dev hub default user...${reset}"
sfdx config:set defaultdevhubusername=$devhubuser
echo "${green}PLEASE ENTER YOUR NEW SCRATCH ORG'S ALIAS:${reset}"
read scratchOrgAlias
createScratchorg "$devhubuser" "$scratchOrgAlias" &
passedPID=$!
loader "$passedPID" && sfdx config:set defaultusername=$devhubuser"."$scratchOrgAlias 
exit 1
