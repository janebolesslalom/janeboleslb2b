global class B2BCheckoutController {

    @AuraEnabled
    public static Map<String, Object> getPaymentInfo(String cartId, String communityId) {
        //Get the webStoreId
        String webstoreId = resolveCommunityIdToWebstoreId(communityId);
        // Get the effectiveAccountId
        String accountId = getUserAccountID(cartId);

        // Get the 'purchaseOrderNumber' from the WebCart
        ConnectApi.CartSummary cartData = ConnectApi.CommerceCart.getCartSummary(
                webstoreId,
                accountId,
                cartId);
        Map<String, Object> paymentInfo = new Map<String, Object>();
        paymentInfo.put('purchaseOrderNumber', cartData.purchaseOrderNumber);

        // Get the billingAddresses
        List<Map<String, Object>> addresses = getAddresses(cartId, accountId);
        paymentInfo.put('addresses', addresses);

        return paymentInfo;
    }

    /**
    * @description Given a community ID, returns the relavent webstore ID for use in B
    * @param communityId The Id of the community from which the call originated
    * @return weStoreId The webStoreId corresponding to the community Id.
    */
    public static String resolveCommunityIdToWebstoreId(String communityId) {
        return
        [
                SELECT WebStoreId
                FROM WebStoreNetwork
                WHERE NetworkId = :communityId
                WITH SECURITY_ENFORCED
                LIMIT 1
        ]
                .WebStoreId;
    }
    public static String getUserAccountID(String cartId) {
        String userId = UserInfo.getUserId();
        String contactId = [SELECT ContactId FROM User WHERE Id = :userId].contactId;
        return [SELECT AccountId FROM Contact WHERE Id = :contactId].accountId;
    }

    public static List<Map<String, Object>> getAddresses(String cartId, String accountId) {
        // Get the billingAddresses
        List<ContactPointAddress> addresses = [
                SELECT Id, IsDefault, City, Street, State, Country, PostalCode, GeocodeAccuracy
                FROM ContactPointAddress
                WHERE AddressType = 'Billing' AND ParentId = :accountId
        ];

        // Get the current selected Billing Address from the WebCart
        Address selectedBilling = [
                SELECT BillingAddress
                FROM WebCart
                WHERE Id = :cartId
        ].BillingAddress;

        List<Map<String, Object>> billingAddresses = new List<Map<String, Object>>();
        Boolean selectedSet = !(selectedBilling != null);
        for (ContactPointAddress cpa : addresses) {
            Map<String, Object> bill = new Map<String, Object>();
            bill.put('id', cpa.Id);
            bill.put('name', cpa.Name);
            bill.put('street', cpa.Street);
            bill.put('city', cpa.City);
            bill.put('country', cpa.Country);
            bill.put('postalCode', cpa.PostalCode);
            bill.put('latitude', cpa.Latitude);
            bill.put('longitude', cpa.Longitude);
            bill.put('geocodeAccuracy', cpa.GeocodeAccuracy);
            bill.put('default', cpa.IsDefault);
            bill.put('selected', false);
            if (!selectedSet) {
                if ((cpa.Street).equals(selectedBilling.Street) &&
                        (cpa.City).equals(selectedBilling.City) &&
                        (cpa.Country).equals(selectedBilling.Country) &&
                        (cpa.PostalCode).equals(selectedBilling.PostalCode)) {
                    bill.put('selected', true);
                    selectedSet = true;
                }
            }
            billingAddresses.add(bill);
        }
        return billingAddresses;
    }

// Make an authorization on the credit card
    @AuraEnabled
    public static void setPaymentInfo(String cartId, Map<String, Object> selectedBillingAddress, Map<String, Object> paymentInfo) {
        ConnectApi.AuthorizationRequest authRequest = new ConnectApi.AuthorizationRequest();
        WebCart cart;
        try {
            cart = [
                    SELECT WebStoreId, GrandTotalAmount, AccountId
                    FROM WebCart
                    WHERE Id = :cartId
            ];
            authRequest.amount = cart.GrandTotalAmount;
            authRequest.accountId = cart.AccountId;
            authRequest.comments = 'Authorizing $' + cart.GrandTotalAmount;
            authRequest.effectiveDate = Datetime.now();
            authRequest.currencyIsoCode = UserInfo.getDefaultCurrency();

            // Cannot proceed if paymentGatewayId does not exist
            String paymentGatewayId = getPaymentGatewayId(cart.WebStoreId);
            if (paymentGatewayId == null || paymentGatewayId.equals('')) {
                throw new AuraHandledException('This store is not authorized to process this gatewayid');
            }
            authRequest.paymentGatewayId = paymentGatewayId;
            //authRequest.paymentMethod = getPaymentMethod(paymentInfo, selectedBillingAddress);
            authRequest.paymentMethod = getPaymentMethod(paymentInfo, selectedBillingAddress);
            authRequest.paymentGroup = getPaymentGroup(cartId);

//authRequest.additionalData = new Map<String, String>();

// Authorize Payment with Payments API
            //ConnectApi.AuthorizationResponse authResponse = ConnectApi.Payments.author
            ConnectApi.AuthorizationResponse authResponse = ConnectApi.Payments.authorize(authRequest);

            if (authResponse.error != null) {
                throw new AuraHandledException('AuthResponseError: ' + authResponse.error);
            }

        } catch (ConnectApi.ConnectApiException e) {
            throw new AuraHandledException(e.getMessage());
        } catch (Exception e) {
            throw new AuraHandledException('Message: ' + e.getMessage() + ' --- ' + e.getStackTraceString());
        }
    }

// Set the cc info and the billing address
    private static ConnectApi.AuthApiPaymentMethodRequest getPaymentMethod(Map<String, Object> paymentInfo, Map<String, Object> billingAddress) {

        //ConnectApi.AuthApiPaymentMethodRequest authApiMethod = new ConnectApi.AuthApiP
        ConnectApi.AuthApiPaymentMethodRequest authApiMethod = new ConnectApi.AuthApiPaymentMethodRequest();

        //ConnectApi.CardPaymentMethodRequest paymentMethod = new ConnectApi.CardPayment
        ConnectApi.CardPaymentMethodRequest paymentMethod = new ConnectApi.CardPaymentMethodRequest();
        paymentMethod.cardCategory = ConnectApi.CardCategory.CreditCard;
        paymentMethod.cardHolderName = (String) paymentInfo.get('cardHolderName');
        paymentMethod.cardNumber = (String) paymentInfo.get('cardNumber');
        paymentMethod.cardType = (String) paymentInfo.get('cardType');
        paymentMethod.cvv = (String) paymentInfo.get('cvv');
        paymentMethod.expiryMonth = (Integer) paymentInfo.get('expiryMonth');
        paymentMethod.expiryYear = (Integer) paymentInfo.get('expiryYear');

        authApiMethod.cardPaymentMethod = paymentMethod;

        ConnectApi.AddressRequest address = new ConnectApi.AddressRequest();
        address.street = (String) billingAddress.get('street');
        address.city = (String) billingAddress.get('city');
        address.state = (String) billingAddress.get('state');
        address.country = (String) billingAddress.get('country');
        address.postalCode = (String) billingAddress.get('postalCode');

        //todo: find address / save for future
        authApiMethod.address = address;
        authApiMethod.saveForFuture = false;
        //authApiMethod.id = ''; // PaymentMethod record ID.
        return authApiMethod;
    }
    private static ConnectApi.PaymentGroupRequest getPaymentGroup(String cartId) {
        ConnectApi.PaymentGroupRequest paymentGroup = new ConnectApi.PaymentGroupRequest();
        paymentGroup.createPaymentGroup = true;
        paymentGroup.currencyIsoCode = UserInfo.getDefaultCurrency();

        String orderId = getOrderId(cartId);
        if (orderId != null && !orderId.equals('')) {
            paymentGroup.sourceObjectId = getOrderId(cartId);
        }
        return paymentGroup;
    }

    private static String getOrderId(String cartId) {

        //WHERE WebCartId = :cartId AN
        return [
                SELECT OrderId
                FROM CartCheckoutSession
                WHERE WebCartId = :cartId
        ].OrderId;
    }

    private static String getPaymentGatewayId(String webStoreId) {
        //return [SELECT Integration FROM StoreIntegratedService WHERE ServiceProviderTy
        return [SELECT Integration FROM StoreIntegratedService WHERE ServiceProviderType = :'Flow' and StoreId = :webStoreId].Integration;
    }
}